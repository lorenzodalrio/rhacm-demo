# RHACM GitOps repository

This repository contains basic configurations that can be used to bootstrap [Red Hat Advanced Cluster Management (RHACM)](https://access.redhat.com/documentation/en-us/red_hat_advanced_cluster_management_for_kubernetes/2.10/html/about/index) and start managing your fleet the GitOps way.

Primary goal is to showcase RHACM capabilities and features, but you can use it as a boilerplate for your own policies repository.

## Structure

- [`bootstrap`](bootstrap) folder contains initial configurations to set up the needed bits;
- [`policy-sets`](policy-sets) folder contains the actual policies organized in [Policy Sets](https://access.redhat.com/documentation/en-us/red_hat_advanced_cluster_management_for_kubernetes/2.10/html/governance/governance#policy-set-controller).

[Policy Generator](https://access.redhat.com/documentation/en-us/red_hat_advanced_cluster_management_for_kubernetes/2.10/html/governance/integrate-third-party-policy-controllers#policy-generator) kustomize plugin is leveraged to ease policy writing and management.

## Usage

1. Install RHACM;
2. Clone the repo to your workstation and cd into your local copy;
3. Create the namespace where the policies will be created:
    ```
    oc apply -f bootstrap/policies-ns.yaml
    ```
4. Create the remaining resources:
    ```
    oc apply -f boostrap/
    ```

## External Secrets Operator

This repo leverages the [External Secrets Operator](https://external-secrets.io/latest/) to securely manage secret data.

The policy [`external-secrets-operator`](policy-sets/cluster-hub/external-secrets-operator/) does the deploy for you on the cluster hub.

In fact, an `ExternalSecret` holds the S3 bucket credentials for the Observability RHACM feature. `ExternalSecrets` can be safely versioned in git because they do not contain any sensitive data, just the reference to construct them on cluster is present.

This guide assumes you're using Hashicorp Vault as the secrets backend, deploying Vault is out of scope here.

1. Create a user named `rhacm-demo` in Vault using the `userpass` method;
2. Change the configuration of the vault endpoint [here](policy-sets/cluster-hub/external-secrets-operator/cluster-secrets-store.yaml).
3. Create a secret with the credentials for `rhacm-demo` user:
    ```
    oc -n eso create secret generic demo-vault-user-auth --from-literal=password=YOUR_PASSWORD_HERE
    ```

## Policy Sets

There are two Policy Sets in this repo:

- [all-managed-clusters](policy-sets/all-managed-clusters/): contains configurations applied to all the managed clusters but the cluster hub.
- [cluster-hub](policy-sets/cluster-hub/): contains configurations applied to the cluster hub.
